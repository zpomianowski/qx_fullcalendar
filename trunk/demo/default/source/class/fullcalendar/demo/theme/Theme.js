/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("fullcalendar.demo.theme.Theme",
{
  meta :
  {
    color : fullcalendar.demo.theme.Color,
    decoration : fullcalendar.demo.theme.Decoration,
    font : fullcalendar.demo.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : fullcalendar.demo.theme.Appearance
  }
});