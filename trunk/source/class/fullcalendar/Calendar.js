/* ************************************************************************

   Copyright:
     2009 ACME Corporation -or- Your Name, http://www.example.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Your Name (username)

************************************************************************ */

/**
 * This is the main class of contribution "fullcalendar"
 *
 * TODO: Replace the sample code of a custom button with the actual code of
 * your contribution.
 *
 * @asset(fullcalendar/*)
 */
qx.Class.define("fullcalendar.Calendar",
{
    extend : dynloadfiles.Contribution,

  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  */

  /**
   * Create a new custom button
   *
   * @param label {String} Label to use
   * @param icon {String?null} Icon to use
   */
    construct : function(settings, codeArr, cssArr, base_path)
    {
        this.__settings = settings;
        codeArr = codeArr || [
            'fullcalendar/fullcalendar-2.3.1/lib/moment.min.js',
            'fullcalendar/fullcalendar-2.3.1/lib/jquery-ui.custom.min.js',
            'fullcalendar/fullcalendar-2.3.1/fullcalendar.min.js',
            'fullcalendar/fullcalendar-2.3.1/lang-all.js'];
        cssArr = cssArr || ['fullcalendar/fullcalendar-2.3.1/fullcalendar.min.css'];
        var base_path = base_path || qx.core.Init.getApplication().gServer + "../static/";
        this.base(arguments, codeArr, cssArr, base_path);

        this.__menu = {};
        this.setContextMenu(this.__getMenu());
    },

    members: {
        __idx: null,
        __jqEl: null,
        __settings: null,
        __menu: null,
        __currentEventId: null,

        __getMenu: function() {
            var menu = new qx.ui.menu.Menu();

            var calcButton = new qx.ui.menu.Button(this.tr("Calculate time"),
                "atms/refresh.png");
            var setTimeButton = new qx.ui.menu.Button(this.tr("Set time manually"),
                "atms/refresh.png");


            calcButton.addListener("click", function() {
                var req = new qx.io.request.Xhr(qx.core.Init.getApplication().gServer + "calculate_test_duration", 'PUT');
                req.setRequestData({
                    id: this.__currentEventId
                });
                req.setTimeout(2400000);
                req.addListener("success", function(e) {
                    this.__jqEl.fullCalendar('refetchEvents');
                }, this);
                req.send();
            }, this);
            setTimeButton.addListener("click", function() { alert('TODO'); }, this);

            this.__menu['calcButton'] = calcButton;
            this.__menu['setTimeButton'] = setTimeButton;

            menu.add(calcButton);
            menu.add(new qx.ui.menu.Separator());
            menu.add(setTimeButton);

            return menu;
        },

        addEventSource: function(source) {
            jQuery(this.__idx).fullCalendar('addEventSource', source);
        },

        removeEventSource: function(source) {
            jQuery(this.__idx).fullCalendar('removeEventSource', source);
        },

        _onElementLoaded: function(el) {
            var idx = "atms-cal-" + dynloadfiles.Contribution.INSTANCE_COUNTER;
            this.__idx = "#" + idx;
            el.setAttribute("id", idx);
            var that = this;
            this.__jqEl = jQuery(this.__idx).fullCalendar({
                lang: qx.locale.Manager.getInstance().getLanguage(),
                // minTime: '9:00:00',
                // maxTime: '17:00:00',
                // weekends: false,
                header:{
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultView: 'agendaWeek',
                eventSources: this.__settings.sources,
                eventDrop: function(event, jsEvent, ui, view) {
                    that.__settings.eventDrop(event, jsEvent, ui, view);
                },
                eventRender: function(calEvent, element, icon) {
                    element.bind('mousedown', function(e) {
                        if (e.which == 3) {
                            that.__currentEventId = calEvent.id;
                        } else {
                            that.__currentEventId = null;
                        }

                    });
                }
            });

            this.addListener('resize', function(e) {
                var data = e.getTarget().getBounds();
                var w = data.width;
                var h = data.height;
                var aspectRatio = parseFloat(w)/h;

                jQuery(this.__idx).fullCalendar('option', 'height', h);
            }, this);
        },

        _destroy: function() {
            jQuery(this.__idx).fullCalendar('destroy');
        }
    }
});
