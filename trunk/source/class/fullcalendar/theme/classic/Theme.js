/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("fullcalendar.theme.classic.Theme",
{
  meta :
  {
    color : fullcalendar.theme.classic.Color,
    decoration : fullcalendar.theme.classic.Decoration,
    font : fullcalendar.theme.classic.Font,
    appearance : fullcalendar.theme.classic.Appearance,
    icon : qx.theme.icon.Oxygen
  }
});