/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("fullcalendar.theme.modern.Theme",
{
  meta :
  {
    color : fullcalendar.theme.modern.Color,
    decoration : fullcalendar.theme.modern.Decoration,
    font : fullcalendar.theme.modern.Font,
    appearance : fullcalendar.theme.modern.Appearance,
    icon : qx.theme.icon.Tango
  }
});